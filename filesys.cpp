#include <iostream>
#include <ctime>
#include <bits/stdc++.h>

using namespace std;

#define MAX_TOT_BLOCK 1500
#define MAX_TOT_INODE 20
#define MAX_DATA_BLOCK 1300
#define SIZE_PER_BLOCK 256
#define FDT_SIZE 50

struct superblock
{
    int fsize; //filesystem size
    int max_db; //max no of data blocks
    int use_db; //no of datablocks used

    int max_fb; //max no of free blocks
    bitset<MAX_TOT_BLOCK> freeblockset;
    int ind_fb;   //next free block index

    int max_inode; // max no of inodes
    int use_inode; //no of inodes used
    bitset<MAX_TOT_INODE> inodeset;
    int ind_inode; // next free inode index
    int sys_max; //The first free block that can be used
    int pwd; //present working directory inode
};


struct inodes
{
    bool filetype;
    int fsize;
    time_t time_last_mod;
    time_t time_last_read;
    time_t time_inode_mod;
    int acc_per;
    int parent_inode;//to store parent directories
    char *dir[8];
    char *indir;
    char *db_indir;

};

struct inode_list
{
    inodes block[MAX_TOT_INODE];
};

struct fdt
{
    int table[FDT_SIZE][3];
    int ffi; //first free index
};

superblock *spb;
inode_list *ilist;
char *db;
fdt t;

void error(string str)
{
    cout << str <<endl;
    exit(0);
}

int min(int a, int b)
{
    if(a<=b)
        return a;
    else
        return b;
}

int gif(int size) //calculates the number of 256 blocks required for a given size
{
    int temp = size/SIZE_PER_BLOCK;
    if(size%SIZE_PER_BLOCK == 0)
        return temp;
    else
        return temp + 1;
}

int char2int(char *fin, int ind)
{
    int tot = 0, idx, placeval, fac = 1;
    char* start = fin + 3 + ind;
    for (idx = 0; idx < 4; idx ++, start --) {
        placeval = (int)(*start);
        if (placeval < 0)
            placeval = placeval + 256;
        tot +=  placeval * fac;
        fac = fac << 8;
    }
    return tot;
}

void int2char(char *arr, int ind, int val) {
    int fac = 1 << 8, idx;
    for (idx = 3; idx >= 0; idx --) {
        arr[idx+ind] = (char)(val % fac);
        val /= fac;
    }
}


void inode_initialiser(inode_list *ilist, int ind) //initialises all inode pointers to 0
{
    ilist->block[ind].indir = NULL;
    ilist->block[ind].db_indir = NULL;
    for(int i =0; i<8; i++)
    {
        ilist->block[ind].dir[i] = NULL;
    }
}


void update_sb(superblock *s, bool datablock = 0, bool inode_update = 0)  //update the superblock parameters
{
    if(inode_update)
    {
        s->inodeset[s->ind_inode] = 1;
        while(s->inodeset[s->ind_inode] == 1)
        {
            s->ind_inode++;
        }
        s->use_inode++;
        return;
    }
    s->freeblockset[s->ind_fb] = 1;
    while(s->freeblockset[s->ind_fb] == 1)
    {
        s->ind_fb++;
    }
    if(datablock)
        s->use_db++;
}

void update(superblock *s, int size, bool datablock = 0, bool inode_update = 0) //update spb parameters for any size
{
    int blocks = gif(size);
    for(int i=0; i<blocks; i++)
        update_sb(s,datablock,inode_update);
}

void directory_maintainer(char *filename, int inode) //maintains the directory of any newly created file
{
    int ind = spb->pwd, i, dbind, index;
    char *start, arr[4], *indir;
    i = ilist->block[ind].fsize/32+1;
    if(gif(ilist->block[ind].fsize)<=8)
        start = ilist->block[ind].dir[gif(ilist->block[ind].fsize)-1];
    else
        start = ilist->block[ind].indir;
    if (i <= 8)
    {
        strcpy(start+(i-1)*32, filename);
        int2char(arr, 0, inode);
        memcpy(start+(i-1)*32+30, arr+2, 2);
    }
    else if(i<=64)
    {
        if(i%8 == 1)
        {
            dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
            start = db + dbind;
            (ilist->block[ind].dir[i/8]) = start;
            update(spb, SIZE_PER_BLOCK, 1);
        }
        strcpy(start+((i-1)%8)*32, filename);
        int2char(arr, 0, inode);
        memcpy(start+((i-1)%8)*32+30, arr+2, 2);
    }
    else if(i<=64+SIZE_PER_BLOCK/4)
    {
        if(i==65)
        {
            dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
            start = db + dbind;
            update(spb, SIZE_PER_BLOCK, 1);
            (ilist->block[ind].indir)= start;
            //*(ilist->block[ind].indir) = dbind;
        }
        if((i-64)%8 == 1)
        {
            indir = start;
            dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
            start = db + dbind;
            update(spb, SIZE_PER_BLOCK, 1);
            int2char(indir, ((i-65)/8)*4, dbind);
        }
        //memcpy(start, readbuf, read_return);
        index = char2int(start,((i-65)/8)*4);
        start = db + index + ((i-65)%8)*32 ;
        strcpy(start, filename);
        int2char(arr, 0, inode);
        memcpy(start+30, arr+2, 2);
    }
    ilist->block[ind].fsize+=32;
    ilist->block[ind].time_inode_mod = ilist->block[ind].time_last_read = ilist->block[ind].time_last_mod = time(0);
}

void mkdir_myfs(char *dirname) //makes a new directory
{
    int ind = spb->ind_inode, dbind;
    char *start, arr[4];
    ilist->block[ind].filetype = 1;
    ilist->block[ind].fsize = 32;
    ilist->block[ind].parent_inode = spb->pwd;

    update_sb(spb, 0,  1);
    inode_initialiser(ilist, ind);

    dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
    start = db + dbind;
    (ilist->block[ind].dir[0])= start;
    update(spb, SIZE_PER_BLOCK, 1);

    strcpy(start, dirname);
    int2char(arr, 0, ind);
    memcpy(start+30, arr+2, 2);

    ilist->block[ind].time_inode_mod = ilist->block[ind].time_last_read = ilist->block[ind].time_last_mod = time(0);
    ilist->block[ind].acc_per = 0775;
    if(ind!=0)
        directory_maintainer(dirname, ind);
}

void initialise_fdt()
{
    for(int i = 0; i<FDT_SIZE; i++)
    {
        t.table[i][0] = t.table[i][1] = 0;
        t.table[i][2] = -1;
    }
    t.ffi = 0;
}
int dbsize = 0;
int create_myfs(int size) //creates the my filesystem
{
    spb = (superblock *)malloc(sizeof(superblock));
    //cout << sizeof(superblock) <<endl;
    if(spb == NULL) return -1;

    int tot_block = gif(size*1024*1024);
    spb->fsize = size*1024*1024;
    spb->max_db = MAX_DATA_BLOCK;
    spb->use_db = 0;
    spb->max_fb = tot_block;
    spb->max_inode = MAX_TOT_INODE;
    spb->use_inode = 0;
    spb->ind_inode = 0;
    spb->ind_fb = 0;
    spb->pwd = 0;
    update(spb, sizeof(superblock));

    ilist = (inode_list*)malloc(sizeof(inode_list));
    if(spb == NULL) return -1;

    spb->ind_fb += gif(sizeof(inode_list));
    update(spb, sizeof(inode_list));
    spb->sys_max = spb->ind_fb;
    dbsize = size*1024*1024 - sizeof(inode_list) - sizeof(superblock);
    db = (char *)malloc(dbsize);
    if(db == NULL) return -1;

    printf("total blocks: %d, first free block index :%d\n", tot_block, spb->ind_fb);
    mkdir_myfs("root");
    initialise_fdt();
    return 0;
}

void block_calc(FILE *filefd, int *a) //calculates file_size and no of blocks reqd
{
    fseek(filefd, 0, SEEK_END);
    a[0] = ftell(filefd);
    rewind(filefd);
    a[1] = gif(a[0]);
}


int copy_pc2myfs(char *source, char *dest)
{
    FILE *fd = fopen(source, "rb");
    if(fd == NULL) error("File opening problem\n");
    //else cout <<"Ho gaya\n";
    int read_return, dbind, count = 1, ptrcount = 0, indiradd, ddindiradd;
    char readbuf[SIZE_PER_BLOCK];
    char *start, *indirst, *ddindirst;

    int a[2];
    block_calc(fd, a);

    int ind = spb->ind_inode;
    ilist->block[ind].filetype = 0;
    ilist->block[ind].fsize = a[0];
    ilist->block[ind].parent_inode = spb->pwd;

    update_sb(spb, 0, 1);

    inode_initialiser(ilist, ind);
    directory_maintainer(dest, ind);

    for(int i = 1; i<=a[1]; i++)
    {
        read_return = fread(readbuf, 1, SIZE_PER_BLOCK, fd);
        dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
        start = db + dbind;
        update(spb, SIZE_PER_BLOCK, 1);
        if (i <= 8)
        {
            memcpy(start, readbuf, read_return);
            (ilist->block[ind].dir[i-1])= start;
            //*(ilist->block[ind].dir[i-1])= dbind; //
        }
        else if(i<=8+SIZE_PER_BLOCK/4)
        {
            if(i==9)
            {
                indiradd = dbind;
                indirst = start;
                (ilist->block[ind].indir)= indirst;
                //*(ilist->block[ind].indir) = dbind;

                dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
                start = db + dbind;
                update(spb, SIZE_PER_BLOCK, 1);
            }
            memcpy(start, readbuf, read_return);
            //db[indiradd +(i-9)*4] = dbind;
            int2char(db, indiradd +(i-9)*4, dbind);
        }
        else
        {
            if(i==8+SIZE_PER_BLOCK/4+1)
            {
                ddindiradd = dbind;
                ddindirst = start;
                (ilist->block[ind].db_indir)= ddindirst;
                //*(ilist->block[ind].db_indir) = dbind;

                dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
                //start = db + dbind;
                update(spb, SIZE_PER_BLOCK, 1);
                //indirst = start;
                indiradd = dbind;
                //db[ddindiradd + ptrcount*4] = dbind;
                int2char(db, ddindiradd + ptrcount*4, dbind);
                ptrcount++;

                dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
                start = db + dbind;
                update(spb, SIZE_PER_BLOCK, 1);
            }
            if(count>SIZE_PER_BLOCK/4)
            {
                //indirst = start;
                indiradd = dbind;
                int2char(db, ddindiradd + ptrcount*4, dbind);
                //db[ddindiradd + ptrcount*4] = dbind;
                ptrcount++;

                dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
                start = db + dbind;
                update(spb, SIZE_PER_BLOCK, 1);
                count = 1;
            }
            memcpy(start, readbuf, read_return);
            int2char(db, indiradd + (count-1)*4, dbind);
            //db[indiradd + (count-1)*4] = dbind;
            count++;
        }
    }

    ilist->block[ind].time_inode_mod = ilist->block[ind].time_last_read = ilist->block[ind].time_last_mod = time(0);
    ilist->block[ind].acc_per = 0664;
    fclose(fd);

}

char* index_calc(int dirblock, int ind) //calculates the pointer where a file is stored in a given directory
{
    if(dirblock < 64)
        return (ilist->block[ind].dir[dirblock/8] + (dirblock%8)*32);
    else
    {
        char *start;
        int index;
        start = ilist->block[ind].indir;
        index = char2int(start, ((dirblock-64)/8)*4);
        return db + index + ((dirblock-64)%8)*32;
    }
}

void deleter_update(superblock *s, int index) //updates spd on data block deletion
{
    s->freeblockset[s->sys_max+index] = 0;
    s->ind_fb = min(s->ind_fb, s->sys_max+index);
    s->use_db--;
}

void del_from_dir(int i, int ind) //deletes a file from directory i represents block no
{
    int block;
    block = ilist->block[ind].fsize/32 - 1;
    char *dest, *source;
    dest = index_calc(i, ind);
    source = index_calc(block, ind);

    memcpy(dest, source, 32);
    if(block<64)
    {
        if(block%8 == 0)
            deleter_update(spb, (ilist->block[ind].dir[block/8] - db)/SIZE_PER_BLOCK);
    }
    else
    {
        if((block-64)%8 == 0)
            deleter_update(spb, (char2int(ilist->block[ind].indir, ((i-64)/8)*4)/SIZE_PER_BLOCK));
        if(block == 64)
            deleter_update(spb, (ilist->block[ind].indir - db)/SIZE_PER_BLOCK);
    }
    ilist->block[ind].fsize-=32;
    ilist->block[ind].time_last_mod = time(0);
}

int* inode_calc(char *filename, int *retval)  //calculates the inode of a file given its name
{
    int ind = spb->pwd, i, block, index, inode = 0;

    char *start, arr[4];
    arr[0] = '\0';
    arr[1] = '\0';
    block = ilist->block[ind].fsize/32;

    for(i = 1; i<=block; i++)
    {
        if (i <= 64)
        {
            start = ilist->block[ind].dir[(i - 1) / 8];
            start = start + ((i-1) % 8) * 32;
            if (!strncmp(filename,start , strlen(filename)))
            {
                arr[2] = start[30];
                arr[3] = start[31];
                inode = char2int(arr, 0);
                break;
            }
        }
        else
        {
            start = ilist->block[ind].indir;
            index = char2int(start, ((i - 65) / 8) * 4);
            start = db + index + ((i - 65) % 8) * 32;
            if (!strncmp(filename, start , strlen(filename)))
            {
                arr[2] = start[30];
                arr[3] = start[31];
                inode = char2int(arr, 0);
                break;
            }
        }
    }
    retval[0] = inode;
    retval[1] = i;
}

int copy_myfs2pc(char *source, char *dest)
{
    FILE *fd = fopen(dest, "wb");
    if(fd == NULL) error("File opening problem\n");

    int ind , retval[2], count = 1, currptr = 0, index, block_size;
    inode_calc(source, retval);
    ind = retval[0];
    int blocks = gif(ilist->block[ind].fsize);
    int tempsize = ilist->block[ind].fsize;
    char buffer[SIZE_PER_BLOCK];

    for(int i=0; i< blocks; i++)
    {
        if(i<8)
        {
            bzero(buffer, SIZE_PER_BLOCK);
            block_size = min(SIZE_PER_BLOCK, tempsize);
            memcpy(buffer,  (ilist->block[ind].dir[i]) , block_size);
            fwrite(buffer, 1, block_size, fd);
            tempsize-=SIZE_PER_BLOCK;
        }
        else if(i<8+SIZE_PER_BLOCK/4)
        {
            bzero(buffer, SIZE_PER_BLOCK);
            block_size = min(SIZE_PER_BLOCK, tempsize);
            index = char2int(ilist->block[ind].indir, (i-8)*4);
            memcpy(buffer, db + index, block_size);
            fwrite(buffer, 1, block_size, fd);
            tempsize -= SIZE_PER_BLOCK;
        }
        else
        {
            index = char2int(ilist->block[ind].db_indir, currptr*4);
            bzero(buffer, SIZE_PER_BLOCK);
            block_size = min(SIZE_PER_BLOCK, tempsize);
            index = char2int(db,index + (count -1)*4);
            memcpy(buffer, db + index, block_size);
            fwrite(buffer, 1, block_size, fd);
            count++;
            tempsize -= SIZE_PER_BLOCK;

            if(count > SIZE_PER_BLOCK/4)
            {
                currptr++;
                count = 1;
            }
        }
    }
    ilist->block[ind].time_last_read = time(0);

    fclose(fd);

}


void inode_del(superblock *s, int ind) //updates spb on inode deletion
{
    s->inodeset[ind] = 0;
    s->ind_inode = min(s->ind_inode, ind);
    s->use_inode--;
}

int rm_myfs(char *filename)
{
    int ind, count = 1, currptr = 0, index, retval[2];
    inode_calc(filename, retval);
    ind = retval[0];
    int blocks = gif(ilist->block[ind].fsize);


    for(int i=0; i< blocks; i++)
    {
        if(i<8)
            deleter_update(spb, (ilist->block[ind].dir[i] - db)/SIZE_PER_BLOCK);

        else if(i<8+SIZE_PER_BLOCK/4)
        {
            if(i==8)
                deleter_update(spb, (ilist->block[ind].indir - db)/SIZE_PER_BLOCK);

            index = char2int(ilist->block[ind].indir, (i-8)*4);
            deleter_update(spb, index/SIZE_PER_BLOCK);
        }
        else
        {
            if(i==8+SIZE_PER_BLOCK/4+1)
                deleter_update(spb, (ilist->block[ind].db_indir - db) / SIZE_PER_BLOCK);

            index = char2int(ilist->block[ind].db_indir, currptr*4);
            if(count == 1)
                deleter_update(spb, index/SIZE_PER_BLOCK);

            index = char2int(db,index + (count -1)*4);
            deleter_update(spb, index/SIZE_PER_BLOCK);
            count++;
            if(count > SIZE_PER_BLOCK/4)
            {
                currptr++;
                count = 1;
            }
        }
    }
    inode_del(spb, ind);
    del_from_dir(retval[1]-1, ilist->block[ind].parent_inode);
}
int check_open(int ind, char mode)
{
    for(int i=0; i<FDT_SIZE; i++)
    {
        if(t.table[i][0] == ind)
        {
            if(t.table[i][2] == 1)
                return 1;
            else
            {
                if(mode == 'r')
                    return 0;
                else
                    return 1;
            }
        }
    }
    return 0;
}

int update_fdt(int ind, char mode)
{
    int ffind = t.ffi;
    t.table[t.ffi][0] = ind;
    t.table[t.ffi][2] = mode == 'w' ? 1 : 0;
    while(t.table[t.ffi][0] != 0)
    {
        t.ffi++;
    }
    return ffind;
}

int open_myfs(char *filename, char mode)
{
    int ind, retval[2];
    inode_calc(filename, retval);
    ind = retval[0];
    if(check_open(ind, mode))
        return -1;
    if(mode == 'w')
    {
        if(ind != 0)
            rm_myfs(filename);


        ind = spb->ind_inode;
        ilist->block[ind].filetype = 0;
        ilist->block[ind].fsize = 0;
        ilist->block[ind].parent_inode = spb->pwd;

        update_sb(spb, 0, 1);

        inode_initialiser(ilist, ind);
        directory_maintainer(filename, ind);
        ilist->block[ind].time_inode_mod = ilist->block[ind].time_last_read = ilist->block[ind].time_last_mod = time(0);
        ilist->block[ind].acc_per = 0664;
        int fd = update_fdt(ind, mode);
        return fd;
    }
    if(mode == 'r')
    {
        if(ind == 0)
            error("File doesn't exist");
        ilist->block[ind].time_last_read = time(0);
        int fd = update_fdt( ind, mode);
        return fd;
    }
}

int close_myfs(int fd)
{
    t.table[fd][0] = t.table[fd][1] = 0;
    t.table[fd][2] = -1;
    t.ffi = min(t.ffi, fd);
}

int read_myfs(int fd, int nbytes, char *buff)
{
    int blocks, currbl, ind, tc = 0;
    ind = t.table[fd][0];
    blocks = min(gif(ilist->block[ind].fsize), gif(t.table[fd][1] + nbytes));
    currbl = gif(t.table[fd][1]);
    if(currbl == 0)
        currbl = 1;

    int count = 1, currptr = 0, index, block_size;
    int tempsize = ilist->block[ind].fsize - t.table[fd][1];
    int offset = t.table[fd][1];
    char buffer[SIZE_PER_BLOCK];

    for(int i=currbl; i<= blocks; i++)
    {
        if(i<=8)
        {
            block_size = min((SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK), tempsize);
            block_size = min(block_size, nbytes);
            memcpy(buff+tc,  (ilist->block[ind].dir[i-1])+offset%SIZE_PER_BLOCK, block_size);
            tc+= block_size;
            offset+=block_size;
            nbytes-=block_size;
            tempsize-=block_size;
        }
        else if(i<=8+SIZE_PER_BLOCK/4)
        {
            block_size = min((SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK), tempsize);
            block_size = min(block_size, nbytes);
            index = char2int(ilist->block[ind].indir, (i-9)*4);
            memcpy(buff+tc, db + index + offset%SIZE_PER_BLOCK, block_size);
            tc+= block_size;
            offset+=block_size;
            nbytes-=block_size;
            tempsize -= block_size;
        }
        else
        {
            index = char2int(ilist->block[ind].db_indir, currptr*4);
            block_size = min((SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK), tempsize);
            block_size = min(block_size, nbytes);
            index = char2int(db,index + (count -1)*4);
            memcpy(buff+tc, db + index + offset%SIZE_PER_BLOCK, block_size);
            tc+= block_size;
            offset+=block_size;
            nbytes-=block_size;
            count++;
            tempsize -= block_size;

            if(count > SIZE_PER_BLOCK/4)
            {
                currptr++;
                count = 1;
            }
        }
    }
    t.table[fd][1] = offset;
    ilist->block[ind].time_last_read = time(0);

}

int write_myfs(int fd, int nbytes, char *buff)
{
    int read_return, dbind, count = 1, ptrcount = 0, indiradd, ddindiradd, currblock, blocks, ind;
    char readbuf[SIZE_PER_BLOCK];
    char *start, *indirst, *ddindirst;

    int offset = t.table[fd][1], tc = 0;
    ind = t.table[fd][0];
    currblock = gif(offset);
    if(currblock == 0)
        currblock = 1;
    blocks = gif(offset + nbytes);

    for(int i = currblock; i<=blocks; i++)
    {
        read_return = min(nbytes, SIZE_PER_BLOCK);
        if(offset%SIZE_PER_BLOCK == 0)
        {
            //read_return = fread(readbuf, 1, SIZE_PER_BLOCK, fd);
            dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
            start = db + dbind;
            update(spb, SIZE_PER_BLOCK, 1);
        }
        else
        {
            if(i<=8)
                start = (ilist->block[ind].dir[i-1]);
            else if(i<=8+SIZE_PER_BLOCK/4)
            {
                int id = char2int(ilist->block[ind].indir, (i-9)*4);
                start = db+id;

            }
        }
        if (i <= 8)
        {
            memcpy(start+offset%SIZE_PER_BLOCK, buff+tc, min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK)));
            nbytes -= min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK));
            tc += min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK));
            offset += min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK));
            (ilist->block[ind].dir[i-1])= start;
            //*(ilist->block[ind].dir[i-1])= dbind; //
        }
        else if(i<=8+SIZE_PER_BLOCK/4)
        {
            if(i==9)
            {
                indiradd = dbind;
                indirst = start;
                (ilist->block[ind].indir)= indirst;
                //*(ilist->block[ind].indir) = dbind;

                dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
                start = db + dbind;
                update(spb, SIZE_PER_BLOCK, 1);
            }
            memcpy(start+offset%SIZE_PER_BLOCK, buff+tc, min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK)));
            nbytes -= min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK));
            tc += min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK));
            offset += min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK));
            //db[indiradd +(i-9)*4] = dbind;
            int2char(db, indiradd +(i-9)*4, dbind);
        }
        else
        {
            if(i==8+SIZE_PER_BLOCK/4+1)
            {
                ddindiradd = dbind;
                ddindirst = start;
                (ilist->block[ind].db_indir)= ddindirst;
                //*(ilist->block[ind].db_indir) = dbind;

                dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
                //start = db + dbind;
                update(spb, SIZE_PER_BLOCK, 1);
                //indirst = start;
                indiradd = dbind;
                //db[ddindiradd + ptrcount*4] = dbind;
                int2char(db, ddindiradd + ptrcount*4, dbind);
                ptrcount++;

                dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
                start = db + dbind;
                update(spb, SIZE_PER_BLOCK, 1);
            }
            if(count>SIZE_PER_BLOCK/4)
            {
                //indirst = start;
                indiradd = dbind;
                int2char(db, ddindiradd + ptrcount*4, dbind);
                //db[ddindiradd + ptrcount*4] = dbind;
                ptrcount++;

                dbind = (spb->ind_fb - spb->sys_max) * SIZE_PER_BLOCK;
                start = db + dbind;
                update(spb, SIZE_PER_BLOCK, 1);
                count = 1;
            }
            memcpy(start+offset%SIZE_PER_BLOCK, buff+tc, min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK)));
            nbytes -= min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK));
            tc += min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK));
            offset += min(read_return, (SIZE_PER_BLOCK - offset%SIZE_PER_BLOCK));
            int2char(db, indiradd + (count-1)*4, dbind);
            //db[indiradd + (count-1)*4] = dbind;
            count++;
        }
    }

    t.table[fd][1]=offset;
    ilist->block[ind].fsize = offset;
    ilist->block[ind].time_inode_mod = ilist->block[ind].time_last_read = ilist->block[ind].time_last_mod = time(0);
}

int eof_myfs(int fd)
{
    int ind = t.table[fd][0];
    if(ilist->block[ind].fsize == t.table[fd][1])
        return 1;
    else
        return 0;
}

int chdir_myfs(char *dirname)
{
    int ind, retval[2];
    inode_calc(dirname, retval);
    ind = retval[0];
    if(ind == 0)
        error("Directory not found");
    spb->pwd = ind;
}

int rm_dir(char *dirname)
{
    int ind, retval[2];
    inode_calc(dirname, retval);
    ind = retval[0];
    if(ind == 0)
        error("No directory to delete");
    if(ilist->block[ind].fsize!=32)
        error("Please provide empty directory name");
    deleter_update(spb, (ilist->block[ind].dir[0] - db)/SIZE_PER_BLOCK);
    inode_del(spb, ind);
    del_from_dir(retval[1]-1, ilist->block[ind].parent_inode);
}


int dump_myfs(char *dumpfile)
{
    FILE *fp = fopen(dumpfile, "wb");
    int n = fwrite((const void *)spb, 1, sizeof(superblock), fp);
    if(n == -1)
        error("Dumping error");
    n = fwrite((const void *)ilist, 1, sizeof(inode_list), fp);
    if(n == -1)
        error("Dumping error");
    n = fwrite((const void *)db, 1, dbsize, fp);
    if(n == -1)
        error("Dumping error");
    fclose(fp);
}

int restore_myfs(char *dumpfile)
{
    FILE *fp = fopen(dumpfile, "rb");
    int n = fread(spb, 1, sizeof(superblock), fp);
    if(n == -1)
        error("Restoring error");
    n = fread(ilist, 1, sizeof(inode_list), fp);
    if(n == -1)
        error("Restoring error");
    n = fread(db, 1, dbsize, fp);
    if(n == -1)
        error("Restoring error");
    spb = (superblock *)spb;
    ilist = (inode_list *)ilist;
    db = (char *)db;
    fclose(fp);
}

int countfb()
{
    int count = 0;
    for(int i=0; i< MAX_TOT_BLOCK; i++)
        if(spb->freeblockset[i] == 1)
            count++;
    return count;
}
int status_myfs()
{
    printf("Total file size: %d\n", spb->fsize);
    int c = countfb();
    printf("Occupied filesystem: %d\n", c*SIZE_PER_BLOCK);
    printf("Free filesystem: %d\n", spb->fsize - c*SIZE_PER_BLOCK);
    printf("Total no of files present: %d\n", spb->use_inode);

}

int chmod_myfs(char *name, int mode)
{
    int ind, retval[2];
    inode_calc(name, retval);
    ind = retval[0];
    if(ind == 0)
        error("File/Directory doesn't exist");

    ilist->block[ind].acc_per = mode;
}
int showfile_myfs(char *filename)
{
    int ind, count = 1, retval[2], currptr = 0, index, block_size;
    inode_calc(filename, retval);
    ind = retval[0];
    int blocks = gif(ilist->block[ind].fsize);
    int tempsize = ilist->block[ind].fsize;
    char buffer[SIZE_PER_BLOCK];

    for(int i=0; i< blocks; i++)
    {
        if(i<8)
        {
            bzero(buffer, SIZE_PER_BLOCK);
            block_size = min(SIZE_PER_BLOCK, tempsize);
            memcpy(buffer,  (ilist->block[ind].dir[i]) , block_size);
            buffer[block_size] = '\0';
            cout<< buffer;
            tempsize-=SIZE_PER_BLOCK;
        }
        else if(i<8+SIZE_PER_BLOCK/4)
        {
            //index = *(ilist->block[ind].indir);
            bzero(buffer, SIZE_PER_BLOCK);
            block_size = min(SIZE_PER_BLOCK, tempsize);
            index = char2int(ilist->block[ind].indir, (i-8)*4);
            memcpy(buffer, db + index, block_size);
            buffer[block_size] = '\0';
            cout << buffer;
            tempsize -= SIZE_PER_BLOCK;
        }
        else
        {
            //ddindex = *(ilist->block[ind].db_indir);
            index = char2int(ilist->block[ind].db_indir, currptr*4);
            //index = db[(ddindex+currptr*4)];
            bzero(buffer, SIZE_PER_BLOCK);
            block_size = min(SIZE_PER_BLOCK, tempsize);
            index = char2int(db,index + (count -1)*4);
            memcpy(buffer, db + index, block_size);
            buffer[block_size] = '\0';
            cout << buffer;
            count++;
            tempsize -= SIZE_PER_BLOCK;

            if(count > SIZE_PER_BLOCK/4)
            {
                currptr++;
                count = 1;
            }
        }
    }
    cout << endl;
    ilist->block[ind].time_last_read = time(0);
}

void int2bin(int a, char *arr, int size)
{
    int i=0;
    while(a!=0)
    {
        arr[size -1 - i] = a%2;
        a/=2;
        i++;
    }
}
void access_perm_decode(int acc, char *arr)
{
    char binstr[9];
    bzero(binstr, 9);
    int2bin(acc, binstr, 9);
    for(int i=0; i<9; i++)
    {
        if(i%3 == 0)
        {
            if(binstr[i] == 1)
                arr[i] = 'r';
            else
                arr[i] = '-';
        }
        if(i%3 == 1)
        {
            if(binstr[i] == 1)
                arr[i] = 'w';
            else
                arr[i] = '-';
        }
        if(i%3 == 2)
        {
            if(binstr[i] == 1)
                arr[i] = 'x';
            else
                arr[i] = '-';
        }
    }
}

int ls_myfs()
{
    printf(" Filetype     Filename     Filesize    Time Last Modified  \n");
    int ind = spb->pwd, i, block, index, inode;
    struct tm * timeinfo;
    char *start, arr[4], aperarr[11];
    arr[0] = arr[1] = '\0';
    block = ilist->block[ind].fsize/32;

    for(i = 2; i<=block; i++) //i = 2 if u don't wanna show pwd , else i = 1
    {
        if (i <= 64)
        {
            start = ilist->block[ind].dir[(i - 1) / 8];
            start = start + ((i-1) % 8) * 32;
        }
        else
        {
            start = ilist->block[ind].indir;
            index = char2int(start, ((i - 65) / 8) * 4);
            start = db + index + ((i - 65) % 8) * 32;
        }
        arr[2] = start[30];
        arr[3] = start[31];
        inode = char2int(arr, 0);
        access_perm_decode(ilist->block[inode].acc_per, aperarr+1);
        aperarr[0] = ilist->block[inode].filetype ? 'd' : '-';
        aperarr[10] = '\0';
        timeinfo = localtime (&ilist->block[inode].time_last_mod);
        printf("%s %10s %12d    %s", aperarr, start, ilist->block[inode].fsize, asctime(timeinfo));
    }
}



int main()
{
    int size,k=1;
    cout << "................MRFS Running....................." << endl;
    cout << "Provide a required size for the filesystem in Mbytes" << endl;
    cin >> size;
    if(create_myfs(size) == -1) error("Filesystem not created\n");
    char str[32];

    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/svm12.txt", "a1");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/ActivationKey.txt", "a2");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/CMakeCache.txt", "a3");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/cmake_install.cmake", "a4");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/CMakeLists.txt", "a5");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/CoverLetter2_VishalGupta.pdf", "a6");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/filesys.cpp", "a7");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/Makefile", "a8");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/n.txt", "a9");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/test.cpp", "a10");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/abc.txt", "a11");
    copy_pc2myfs("/home/vishal/Documents/Sem6/OS/Ass04/Expt-1.pdf", "a12");
    ls_myfs();
    cout<<endl;
    //showfile_myfs("a7");
    copy_myfs2pc("a9", "/home/vishal/Documents/Sem6/OS/Ass04/k.txt");
/*
    while(k==1) {
        cout << "Enter the file u wanna delete" << endl;
        cin >> str;
        rm_myfs(str);
        ls_myfs();
        cout << endl;
        cout << "Wanna delete more... type 1 for yes and 0 for no" << endl;
        cin >> k;
    }

    int fd = open_myfs("a11", 'r');
    char buf[5000];
    bzero(buf, 5000);
    read_myfs(fd,1000,buf);
    int fd2 = open_myfs("a8", 'w');
    write_myfs(fd2,1000, buf);
    //cout << buf <<endl;
    bzero(buf, 5000);
    read_myfs(fd,1000,buf);
    write_myfs(fd2,1000, buf);
    //cout << buf <<endl;
    close_myfs(fd);
    close_myfs(fd2);
    showfile_myfs("a8");
    ls_myfs();*/

//    dump_myfs("/home/vishal/Documents/Sem6/OS/Ass04/nback.backup");
//
//    while(k==1) {
//        cout << "Enter the file u wanna delete" << endl;
//        cin >> str;
//        rm_myfs(str);
//        ls_myfs();
//        status_myfs();
//        cout << endl;
//        cout << "Wanna delete more... type 1 for yes and 0 for no" << endl;
//        cin >> k;
//    }
//    status_myfs();
//
//    //restore_myfs("/home/vishal/Documents/Sem6/OS/Ass04/nback.backup");
//    ls_myfs();
//    status_myfs();
//
//    chmod_myfs("a9", 0600);
    ls_myfs();

    mkdir_myfs("dir1");
    ls_myfs();
    rm_dir("dir1");
    ls_myfs();

}